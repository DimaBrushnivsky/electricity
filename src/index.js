export class World {
  // static households = 0;
  // static increase() {
  //   this.households++;
  // }

  constructor(households) {
    // this.households = households;
    // World.increase();
  }

  createPowerPlant() {
    const powerPlant = {
      id: Math.random(),
      electricity: 0,
      isAlive: true,
      connectedHouseholds: [],

      getEnergy() {
        this.electricity = 100;
      },

      produceEnergy(household) {
        if (this.isAlive) {
          let certainHousehold = this.connectedHouseholds
            .findIndex(el => el.id === household.id);

          this.connectedHouseholds[certainHousehold].electricity += this.electricity;
        }

        return 0;
      },

      connectHousehold(household) {
        if (!this.isAlive) {
          return;
        }

        this.connectedHouseholds.push(household);
        this.produceEnergy(household);
      },

      disconnectEnergy(household) {
        this.connectedHouseholds = this.connectedHouseholds
          .filter((el, index) => {
            if (el.id === household.id) {
              this.connectedHouseholds[index].electricity -= this.electricity;
            }

            return el.id !== household.id;
        });
      },

      repairPowerPlant() {
        this.isAlive = true;
        this.connectedHouseholds
          .forEach((el, index) => {
            this.connectedHouseholds[index].electricity += this.electricity;
        });
      },

      killPowerPlant() {
        this.isAlive = false;
        this.connectedHouseholds
          .forEach((el, index) => {
            this.connectedHouseholds[index].electricity -= this.electricity;
        });
      }
    };

    return powerPlant;
  }

  createHousehold() {
    const household = {
      id: Math.random(),
      connectedPowerPlants: [],
      connectedHouseholds: [],
      electricity: 0,

      householdHasEletricity() {
        return !!this.electricity;
      },

      produceEnergytoConnectedHouse(household) {
        const alivePowerPlants = this.connectedPowerPlants
          .some(el => el.isAlive === true);

        if (alivePowerPlants) {
          let certainHousehold = this.connectedHouseholds
            .findIndex(el => el.id === household.id);

          this.connectedHouseholds[certainHousehold].electricity += 100;
        }

        return 0;
      },

      addPowerPlant(powerPlant) {
        this.connectedPowerPlants.push(powerPlant);
      },

      connectHousehold(household) {
        this.connectedHouseholds.push(household);
        this.produceEnergytoConnectedHouse(household);
      },

      updateConnectedPowerPlants(powerPlant) {
        this.connectedPowerPlants = this.connectedPowerPlants
          .filter(el => el.id !== powerPlant.id)
      }
    };

    return household;
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    // if ((household.connectedHouseholds.length + household.connectedPowerPlants.length > 10)
    //   ||
    //   (household.connectedHouseholds.length + household.connectedPowerPlants.length > 100)) {
    //     return;
    // }

    household.addPowerPlant(powerPlant);
    powerPlant.getEnergy();
    powerPlant.connectHousehold(household);
  }

  connectHouseholdToHousehold(household1, household2) {
    household1.connectHousehold(household2);
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    // if (this.households > 10 || this.households > 100) {
    //   powerPlant.disconnectEnergy(household);
    //   household.updateConnectedPowerPlants(powerPlant);
    // }
    powerPlant.disconnectEnergy(household);
    household.updateConnectedPowerPlants(powerPlant);
  }

  killPowerPlant(powerPlant) {
    powerPlant.killPowerPlant();
  }

  repairPowerPlant(powerPlant) {
    powerPlant.repairPowerPlant();
  }

  householdHasEletricity(household) {
    if ((household.connectedHouseholds.length + household.connectedPowerPlants.length > 10)
    ||
    (household.connectedHouseholds.length + household.connectedPowerPlants.length > 100)) {
      return;
  }

    return household.householdHasEletricity();
  }
}
